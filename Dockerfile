FROM docker.io/sagemath/sagemath

COPY . .

EXPOSE 8080

ENV PYTHONPATH=stability_conditions
CMD ["sage", "server.py"]
